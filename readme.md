# Detect Cover Images as Black Color - Read me

## What the repository will do?
Run the repository in your terminal to check the Cover Images is black or not

## How to build?
Run composer update to update the dependences lib. Then, migrate your database with new columns for `covers_uploaded` table
```
#!batch

$cd /path/to/projects/directory
$composer update
$php artisan migrate
```


## How to run?
Run with your terminal is best practice, because of performance is not good when run in Web Browsers directly.

The command will get all records (`success = 1` and `checked = 0`) to check, and mark if the images is black!

```
#!batch

$cd /path/to/projects/directory
$php artisan image:detect
```

Done, check `is_black` and `checked` columns in your `covers_uploaded` table to see the result.
```
#!sql

SELECT * FROM `covers_uploaded` WHERE `success` = 1 AND `is_black` = 1 AND `checked` = 1
```




Best regards.


@nil-vn