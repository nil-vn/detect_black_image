<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\CoversUploadedRepository;
use App\Entities\CoversUploaded;
use App\Validators\CoversUploadedValidator;

/**
 * Class ConversUploadedRepositoryEloquent
 * @package namespace App\Repositories;
 */
class CoversUploadedRepositoryEloquent extends BaseRepository implements CoversUploadedRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return CoversUploaded::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
