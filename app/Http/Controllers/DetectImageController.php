<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Entities\CoversUploaded;
use App\Entities\Products;
use App\Repositories\CoversUploadedRepository;
use App\Repositories\ProductsRepository;
use Symfony\Component\HttpFoundation\Response;
use League\ColorExtractor\Color;
use League\ColorExtractor\ColorExtractor;
use League\ColorExtractor\Palette;

class DetectImageController extends Controller
{
	/**
	 * The user repository instance.
	 */
	protected $covers;
	protected $recordPerTime = 100;

	/**
	 * Create a new controller instance.
	 *
	 * @param  CoversUploadedRepository $covers
	 * @param  ProductsRepository $covers
	 * @return void
	 */
	public function __construct(CoversUploadedRepository $covers)
	{
		$this->covers = $covers;
	}

	/**
	 * Detect Image as Black Color
	 *
	 * @return void
	 */
	public function index()
	{
		$covers = $this->covers
			->findWhere(['success' => 1, 'checked' => 0])
			->take(CoversUploaded::LIMIT);
		foreach ($covers as $cover) {
			$palette = Palette::fromFilename(CoversUploaded::URL . $cover->product->isbn13. CoversUploaded::FILE_EXT);
			$colorCount = count($palette);
			try{
				$this->covers->update([
					'is_black' => ($colorCount == 1) ? 1 : 0,
					'checked' => 1
				], $cover->isbn10);
			} catch(Exception $e) {
				// Handle error records
			}
		}
		return json_encode([
			'status' => 200,
			'data' => []
		]);
	}
}
