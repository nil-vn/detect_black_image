<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Repositories\CoversUploadedRepository;
use App\Repositories\ProductsRepository;
use League\ColorExtractor\Color;
use League\ColorExtractor\ColorExtractor;
use League\ColorExtractor\Palette;
use App\Entities\CoversUploaded;

class DetectImages extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'image:detect';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Detect images as black color';
	protected $covers;
	protected $products;

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct(CoversUploadedRepository $covers, ProductsRepository $products)
	{
		parent::__construct();
		$this->covers = $covers;
		$this->products = $products;
	}

	/**
	 * Execute the console command.
	 *
	 * @author tinhtn <trannt.90@gmail.com>
	 * @return mixed
	 */
	public function handle()
	{
		$covers = $this->covers
			->findWhere(['success' => 1, 'checked' => 0])
			->take(CoversUploaded::LIMIT);

		if($covers->count() == 0) {
			$this->info('Not found the unchecked cover images');
			exit();
		}
		$this->info('Found '.$covers->count().' results');
		$bar = $this->output->createProgressBar(count($covers));
		$bar->setFormat("%message%\n %current%/%max% [%bar%] %percent:3s%%");

		foreach ($covers as $cover) {
			$palette = Palette::fromFilename(CoversUploaded::URL . $cover->product->isbn13. CoversUploaded::FILE_EXT);
			$colorCount = count($palette);
			try{
				$this->covers->update([
					'is_black' 	=> ($colorCount == 1) ? 1 : 0,
					'checked' 	=> 1
				], $cover->isbn10);
				$message = 'ISBN '.$cover->isbn10.(($colorCount > 1) ? ' isn\'t' : 'IS').' black color!';
			} catch(Exception $e) {
				// $this->error('ISBN: '.$cover->isbn10.' - Error: '.$e->getMessage());
				$message = 'ISBN: '.$cover->isbn10.' - Error: '.$e->getMessage();
			}

			$bar->setMessage($message);
			$bar->advance();
		}
		$bar->finish();
	}
}
