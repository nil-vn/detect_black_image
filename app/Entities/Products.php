<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Products extends Model implements Transformable
{
	use TransformableTrait;

	protected $fillable = [];

	protected $primaryKey = 'isbn10';

	public function covers()
	{
		return $this->hasOne('App\Entities\CoversUploaded', 'isbn10');
	}
}
