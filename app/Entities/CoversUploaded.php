<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class CoversUploaded extends Model implements Transformable
{
	use TransformableTrait;

	const URL 		= 'http://avalonworldwide.com/lazada_upload/bookcovers/image/';
	const FILE_EXT 	= '.jpg';
	const LIMIT 	= 1000;

	/**
	 * The table associated with the model.
	 *
	 * @var string
	 */
	protected $table 	= 'covers_uploaded';
	protected $fillable = [
		'is_black',
		'checked'
	];
	protected $primaryKey 	= 'isbn10';
	public $timestamps 		= false;


	public function product()
	{
		return $this->hasOne('App\Entities\Products', 'isbn10');
	}
}
